# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'smartgeoip/version'

Gem::Specification.new do |spec|
  spec.name          = "smartgeoip"
  spec.version       = Smartgeoip::VERSION
  spec.authors       = ["Virp"]
  spec.email         = ["skvirp@gmail.com"]
  spec.description   = %q{Location by IP}
  spec.summary       = %q{Locate city by IP address}
  spec.homepage      = "https://github.com/Virp/smartgeoip"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
