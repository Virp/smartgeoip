require "smartgeoip/version"
require "net/http"
require "rails"

module Smartgeoip
  URL = 'http://ru.smart-ip.net/geoip-json'

  def self.locate(ip, lang='en')
  	uri = URI.parse(URL)
  	uri.query = URI.encode_www_form :host => ip, :lang => lang
  	ActiveSupport::JSON.decode(Net::HTTP.get(uri))
  end
end
