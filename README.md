# Smartgeoip

Locate country, region and city by IP address.

## Installation

Add this line to your application's Gemfile:

    gem 'smartgeoip'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install smartgeoip

## Usage

    location = Smartgeoip.locate('10.11.12.13')
    location["source"]		# => source of geo-IP data
    location["host"]		# => contains IP-address on which was searched geo-data
    location["lang"]		# => query language (for example, en - English, ru - Russian). Use 2-character designation of languages ​​the ISO 639-1
    location["countryName"]	# => name of the country in the language of the query
    location["countryCode"]	# => 2-character country identifier standard ISO 3166-1
    location["city"]		# => the name of the city in the language of the query
    location["region"]		# => name of a region in the language of the request (territorial division in the country, for example - Utah, Kyiv region)
    location["latitude"]	# => the latitude of the approximate center of the city found
    location["longitude"]	# => the longitude of the approximate center of the city found
    location["timezone"]	# => time zone

You can specify the query language

    location = Smartgeoip.locate('10.11.12.13', 'ru')

Default language 'en', supported languages is 'en' and 'ru'

If an error occurs when using the service will return an error:

    location["error"]		# => Will contain, for example: "Cannot resolve host name!"

## Terms of Use

This gem use the http://smart-ip.net
They limit the number of requests per IP-address to the service of 5000 requests per day.
All terms of use and documentation you can read on http://ru.smart-ip.net/geoip-api (Russian).

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
